﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using ImageSharp;

namespace JigsawGenerator
{
	// ReSharper disable once ClassNeverInstantiated.Global
	internal class Program
	{
		private const int PIXELS_WIDE = 4096;
		private const int PIXELS_HIGH = 4096;

		private const int PIECE_HEIGHT = 512;
		private const int PIECE_WIDTH = 512;
		private const int PIECES_WIDE = PIXELS_WIDE/PIECE_WIDTH;
		private const int PIECES_HIGH = PIXELS_HIGH/PIECE_HEIGHT;
		private static readonly int TabWidth = Math.Min(PIECE_HEIGHT, PIECE_WIDTH) / 4;
		private static Action<int, int> plot;
		private static readonly Rgba32 ForegroundColour = Rgba32.Black;
		private static readonly Rgba32 BackgroundColour = Rgba32.White;
		private static TypeOfTab typeOfTab = TypeOfTab.Angled;

		/// <summary>
		/// Note that there isn't  abottom or right line
		/// This is intended. It's because there is no allowed gap between the pieces, and as such, the line exits on the top row of pixels
		/// of each piece, and the left most row of each piece. As such there is no piece at the bottom
		/// To rectify this add an extra row of pixels and colour it.
		/// </summary>
		public static void Main(string[] args)
		{
			if (args.Length != 0)
				if (args[0] == "-a")
					typeOfTab = TypeOfTab.Angled;

			using (Image<Rgba32> image = new Image<Rgba32>(PIXELS_WIDE + 1, PIXELS_HIGH + 1))
			{
				SetupImage(image);

				// ReSharper disable once AccessToDisposedClosure
				plot = (x, y) => image[x, y] = ForegroundColour;
				
				for (int i = 0; i < PIECES_WIDE; i++)
				{
					for (int j = 0; j < PIECES_HIGH; j++)
					{
						Point lineStart = new Point(PIECE_WIDTH * i, PIECE_HEIGHT * j);
						int xLineEnd = PIECE_WIDTH * (i + 1);
						int yLineEnd = PIECE_HEIGHT * (j + 1);

						DrawJigsawEdge(lineStart, new Point(xLineEnd, lineStart.Y), image);
						DrawJigsawEdge(lineStart, new Point(lineStart.X, yLineEnd), image);
					}
				}
				
				image.Save("Jigsaw.png");
			}
			
			Console.WriteLine($"Done");
		}

		/// <summary>
		/// Pain background and bottom and right edge
		/// </summary>
		/// <param name="image"></param>
		private static void SetupImage(Image<Rgba32> image)
		{
			for (int i = 0; i < PIXELS_WIDE; i++)
				for (int j = 0; j < PIXELS_HIGH; j++)
					image[i, j] = BackgroundColour;

			for (int i = 0; i < image.Width; i++)
				image[i, image.Height - 1] = ForegroundColour;
			for (int i = 0; i < image.Height; i++)
				image[image.Width - 1, i] = ForegroundColour;
		}

		private static readonly Random Rng = new Random((int)DateTime.Now.Ticks);
		
		/// <summary>
		/// This draws a jigsaw line. Options are horizontal or vertical
		/// The board is made up of lots of Γ shapes, one vertical line then a horizontal line the move to the next space
		/// </summary>
		/// <param name="start">Top left point of the line</param>
		/// <param name="end">end of the line, bottom left for vert and top right for horiz</param>
		/// <param name="image"></param>
		private static void DrawJigsawEdge(Point start, Point end, Image<Rgba32> image)
		{
			bool upOrRight = Rng.Next(0, 2) == 0;
			//the edge of the jigsaw piece is split into 3 sections, flat, tab and flat
			int line1Start = start.Y == end.Y ? start.X : start.Y;
			int midpoint = start.Y == end.Y ? PIECE_WIDTH / 2 : PIECE_HEIGHT / 2;
			int line1End = line1Start + midpoint - TabWidth / 2 - 1;//line1 is the bit from 0,0 to the tab
			int line2Start = line1End + 1;//line 2 is the tab section
			int line2End = line2Start + TabWidth - 1;
			int line3Start = line2End + 1;//line 3 is the section after the tab
			int line3End = line3Start + midpoint - TabWidth / 2 - 1;
				
			if (start.Y == end.Y)//horizontal
			{
				DrawStraightLine(new Point(line1Start, start.Y), new Point(line1End, start.Y));

				if (start.Y == 0) //edge piece, no tab
					DrawStraightLine(new Point(line2Start, start.Y), new Point(line2End, start.Y));
				else //tab
					DrawTab(image, line2Start + TabWidth / 2, start.Y, upOrRight ? Orientation.Up : Orientation.Down);

				DrawStraightLine(new Point(line3Start, start.Y), new Point(line3End, start.Y));
			}
			else//vert
			{
				DrawStraightLine(new Point(start.X, line1Start), new Point(start.X, line1End));

				if (start.X == 0) //edge piece, no tab
					DrawStraightLine(new Point(start.X, line2Start), new Point(start.X, line2End));
				else //tab
					DrawTab(image, line2Start + TabWidth / 2, start.X, upOrRight ? Orientation.Right : Orientation.Left);

				DrawStraightLine(new Point(start.X, line3Start), new Point(start.X, line3End));
			}
		}

		private static void DrawTab(Image<Rgba32> image, int midPoint, int baseLine, Orientation orientation)
		{
			// ReSharper disable once ConvertIfStatementToSwitchStatement
			if(typeOfTab == TypeOfTab.Curved)
				DrawCurvedTab(image, midPoint, baseLine, orientation);
			else if (typeOfTab == TypeOfTab.Angled)
				DrawAngledTab(midPoint, baseLine, orientation);
		}

		private static void DrawAngledTab(int midpoint, int baseline, Orientation orientation)
		{
			int offset = TabWidth / 2 + TabWidth / 4;
			int multiplier;
			
			if (orientation == Orientation.Up || orientation == Orientation.Left)
				multiplier = -1;
			else
				multiplier = 1;
			
			List<(Point start, Point end)> points = new List<(Point start, Point end)>
			{
				(new Point(midpoint - offset, baseline + TabWidth * multiplier), new Point(midpoint + offset, baseline + TabWidth * multiplier)),
				(new Point(midpoint - offset, baseline + TabWidth * multiplier), new Point(midpoint - TabWidth / 2, baseline)),
				(new Point(midpoint + offset, baseline + TabWidth * multiplier), new Point(midpoint + TabWidth / 2, baseline))
			};

			foreach ((Point start, Point end) point in points)
			{
				if (orientation == Orientation.Left || orientation == Orientation.Right)
					DrawStraightLine(point.start.FlipXAndY(), point.end.FlipXAndY());
				else
					DrawStraightLine(point.start, point.end);
			}
		}
		
		/// <summary>
		/// Draws a basic line. Note that the line must be drawn smallest value to largest value and not in reverse
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		private static void DrawStraightLine(Point start, Point end) => Algorithms.Line(start.X, start.Y, end.X, end.Y, (i, i1) =>
			{
				plot(i, i1);
				return true;
			});

		private static void DrawCurvedTab(Image<Rgba32> image, int midPoint, int baseLine, Orientation orientation)
		{
			int multiplier = orientation == Orientation.Up || orientation == Orientation.Left ? -1 : 1;

			List<Point> pointsSide1 = GetPoints(1);
			List<Point> pointsSide2 = GetPoints(-1);
			
			List<Point> GetPoints(int multiplier1) => new List<Point>
			{
				new Point(midPoint + TabWidth / 2 * multiplier1, baseLine),
				new Point(midPoint + TabWidth / 2 * multiplier1, baseLine + TabWidth / 3 * multiplier),
				new Point(midPoint + TabWidth / 2 * multiplier1 + TabWidth * multiplier1, baseLine + TabWidth * multiplier),
				new Point(midPoint, baseLine + TabWidth * multiplier)
			};

			if(orientation == Orientation.Left || orientation == Orientation.Right)
				for (int i = 0; i < pointsSide1.Count; i++)
				{
					pointsSide1[i] = pointsSide1[i].FlipXAndY();
					pointsSide2[i] = pointsSide2[i].FlipXAndY();
				}
			
			Algorithms.DrawCasteljau(pointsSide1, (x, y) => image[x, y] = ForegroundColour);	
			Algorithms.DrawCasteljau(pointsSide2, (x, y) => image[x, y] = ForegroundColour);
		}
	}
}