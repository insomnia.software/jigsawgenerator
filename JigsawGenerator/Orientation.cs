﻿namespace JigsawGenerator
{
	public enum Orientation
	{
		Up,
		Down,
		Right,
		Left
	}
}