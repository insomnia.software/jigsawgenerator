﻿using System.Drawing;

namespace JigsawGenerator
{
	public static class Extensions
	{
		public static Point FlipXAndY(this Point p) => new Point(p.Y, p.X);

		public static void Flip(this Point p)
		{
			int tmp = p.X;
			p.X = p.Y;
			p.Y = tmp;
		}
	}
}